# Kugelblitz
A pastiche of the golden age of arcade games.

Made in Godot 3.1 for the eighth Godot Wild Jam.

Pixelart was made with Aseprite, sound effects made with LabChirp.

You can download it [here](https://johngabrieluk.itch.io/kugelblitz).

The cover-art was made by [Jodediah Holems](https://twitter.com/jodediah).