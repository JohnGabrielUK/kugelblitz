extends Control

onready var label_name = $Label_Name

onready var current_char := 0
onready var name_entered = [0, 0, 0]
onready var blinking = false

const chars = " ABCDEFGHIJKLMNOPQRSTUVWXYZ"

func update_display() -> void:
	var result = ""
	for i in range(0, 3):
		if current_char == i and blinking:
			result += " "
		elif name_entered[i] == 0:
			result += "_"
		else:
			result += chars.substr(name_entered[i], 1)
	label_name.text = result

func done() -> void:
	# Construct the name as a string
	var result = ""
	for i in range(0, 3):
		result += chars.substr(name_entered[i], 1)
	var ranking = Rankings.Ranking.new(result, Overall.score)
	Rankings.add_ranking(ranking)
	get_tree().change_scene("res://scenes/TitleScreen.tscn")

func _unhandled_input(event):
	if event.is_action_pressed("climb_up"):
		var new_char = name_entered[current_char] + 1
		if new_char >= 27:
			new_char = 0
		name_entered[current_char] = new_char
	if event.is_action_pressed("climb_down"):
		var new_char = name_entered[current_char] - 1
		if new_char < 0:
			new_char = 26
		name_entered[current_char] = new_char
	if event.is_action_pressed("run_left"):
		current_char -= 1
		if current_char < 0:
			current_char = 2
	if event.is_action_pressed("run_right"):
		current_char += 1
		if current_char > 2:
			current_char = 0
	if event.is_action_pressed("jump"):
		done()
	update_display()

func _on_Timer_Blink_timeout() -> void:
	if blinking: blinking = false
	else: blinking = true
	update_display()

func _ready() -> void:
	update_display()
