extends Node2D

const obj_hud = preload("res://objects/HUD.tscn")
const obj_player = preload("res://objects/Player.tscn")
const obj_floppy = preload("res://objects/Floppy.tscn")

onready var hud
onready var player = $Player
onready var music = $Audio_Music
onready var audio_scorecounter = $Audio_ScoreCounter
onready var bonus_positions = $BonusPositions

func update_hud():
	hud.set_label_score(Overall.score)
	hud.set_label_hiscore(Overall.hiscore)
	hud.set_label_lives(Overall.lives)
	hud.set_label_timer(int(Overall.timer))

func reset():
	# First of all, make sure we have extra lives
	Overall.lives -= 1
	update_hud()
	if Overall.lives < 0:
		if Overall.timer > 0:
			get_tree().change_scene("res://scenes/GameOver.tscn")
		else:
			get_tree().change_scene("res://scenes/BlackHole.tscn")
		return
	# If we do, continue merrily on
	get_tree().reload_current_scene()

func is_level_complete():
	for current_scientist in get_tree().get_nodes_in_group("scientist"):
		if Overall.scientists_saved[current_scientist.slug] == false:
			return false
	return true

func finish_level():
	music.stop()
	get_tree().paused = true
	yield(get_tree().create_timer(1.0), "timeout")
	for next_decrement in [10, 1]:
		while Overall.timer > next_decrement:
			Overall.timer -= next_decrement
			Overall.score += next_decrement * 10
			update_hud()
			audio_scorecounter.play()
			yield(get_tree().create_timer(0.02), "timeout")
	yield(get_tree().create_timer(1.0), "timeout")
	get_tree().paused = false
	Overall.next_level()

func scientist_saved(which):
	Overall.scientists_saved[which] = true
	if is_level_complete():
		finish_level()

func player_hurt():
	music.stop()

func add_score(value):
	Overall.score += value
	update_hud()

func _physics_process(delta):
	Overall.timer -= delta * 3
	update_hud()
	# Music speed
	if Overall.timer < 100:
		music.pitch_scale = 1.75
	elif Overall.timer < 200:
		music.pitch_scale = 1.50
	elif Overall.timer < 350:
		music.pitch_scale = 1.25
	else:
		music.pitch_scale = 1.00
	# Time out?
	if Overall.timer <= 0 and Overall.lives >= 0:
		player.get_hurt()
		Overall.lives = -1 # Yes, this ends the game. It is a bit mean
	# Check to spawn bonus
	if Overall.timer < Overall.next_bonus:
		# Pick a random location
		var which_position = rand_range(0, bonus_positions.get_child_count())
		var floppy = obj_floppy.instance()
		floppy.position = bonus_positions.get_child(which_position).position
		add_child(floppy)
		Overall.next_bonus -= 100

func _ready():
	hud = obj_hud.instance()
	add_child(hud)
	update_hud()
	music.play()
	player.connect("scientist_saved", self, "scientist_saved")
	player.connect("player_hurt", self, "player_hurt")
	for current_scientist in get_tree().get_nodes_in_group("scientist"):
		if Overall.scientists_saved[current_scientist.slug] == true:
			current_scientist.set_picked_up()