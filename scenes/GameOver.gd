extends Control

func _on_Timer_MoveOn_timeout():
	if Rankings.score_makes_ranking(Overall.score):
		get_tree().change_scene("res://scenes/NameEntry.tscn")
	else:
		get_tree().change_scene("res://scenes/TitleScreen.tscn")
