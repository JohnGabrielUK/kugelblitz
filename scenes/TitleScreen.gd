extends Control

onready var logo = $TextureRect_Logo
onready var scoreboard = $Scoreboard
onready var label_prompt = $Label_Prompt
onready var label_hiscore = $Label_Hiscore

var phase := 0

func _input(event):
	if event.is_action_pressed("jump"):
		if Overall.credits > 0:
			Overall.credits -= 1
			Overall.new_game()

func credit_added() -> void:
	label_prompt.text = "PRESS BUTTON TO PLAY"

func _ready():
	label_hiscore.text = "%06d" % Rankings.get_highest_ranking_score()
	Overall.connect("credit_added", self, "credit_added")
	_on_Timer_ChangeScreen_timeout()

func _on_Timer_ChangeScreen_timeout():
	if phase == 0:
		logo.hide()
		scoreboard.visible = true
		scoreboard.display()
		phase = 1
	else:
		logo.show()
		scoreboard.visible = false
		phase = 0
