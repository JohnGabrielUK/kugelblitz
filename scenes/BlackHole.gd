extends Node2D

const obj_debris = preload("res://objects/Debris.tscn")

const centre := Vector2(112, 56)

var tile_regions = {
	0: Rect2(0, 0, 8, 8),
	1: Rect2(8, 0, 8, 8),
	2: Rect2(16, 0, 8, 8),
	3: Rect2(24, 0, 8, 8)
}

onready var tilemap = $TileMap
onready var camera = $Camera2D
onready var timer = $Timer_Done
onready var audio_rumble = $Audio_Rumble
onready var audio_bang = $Audio_Bang

onready var next_bang = 1.5

func create_debris():
	for current_tile_type in tile_regions:
		var region = tile_regions[current_tile_type]
		var tiles = tilemap.get_used_cells_by_id(current_tile_type)
		for current_tile in tiles:
			var tile_pos = tilemap.map_to_world(current_tile) + tilemap.position
			var debris = obj_debris.instance()
			debris.set_region(region)
			debris.set_flipped(tilemap.is_cell_x_flipped(current_tile.x, current_tile.y),
					tilemap.is_cell_y_flipped(current_tile.x, current_tile.y))
			debris.destination = centre
			debris.delay = 1 + randf() + centre.distance_to(tile_pos) / 50
			debris.global_position = tile_pos
			add_child(debris)
			debris.set_as_toplevel(true)

func _physics_process(delta):
	camera.position = Vector2(randf()-0.5, randf()-0.5) * 8
	var debris_count = get_tree().get_nodes_in_group("debris").size()
	audio_rumble.pitch_scale = 0.1 + (float(debris_count)/100.0)
	if debris_count <= 0:
		audio_rumble.volume_db -= delta * 10
	# Bangs
	if debris_count > 64:
		next_bang -= delta
		if next_bang <= 0:
			audio_bang.pitch_scale = 0.25 + (randf()/1.5)
			audio_bang.play()
			next_bang = 0.1 + randf()
		

func _ready():
	create_debris()
	audio_rumble.play()
	audio_bang.play()
	timer.start()

func _on_Timer_Done_timeout():
	get_tree().change_scene("res://scenes/GameOver.tscn")
