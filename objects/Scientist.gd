extends Area2D

const obj_score = preload("res://objects/Score.tscn")

const ANIM_SPEED = 5.0

export (float) var anim_frame = 0.0
export (String) var slug

onready var sprite = $Sprite

onready var picked_up = false

func reset():
	picked_up = false
	sprite.show()

func set_picked_up():
	picked_up = true
	sprite.hide()

func pickup():
	picked_up = true
	sprite.hide()
	var score = obj_score.instance()
	score.global_position = global_position
	score.set_point_value(1000)
	get_tree().call_group("game", "add_score", 1000)
	get_parent().add_child(score)

func _physics_process(delta):
	anim_frame += delta * ANIM_SPEED
	sprite.frame = int(anim_frame) % sprite.hframes
