extends KinematicBody2D
class_name Player

const obj_score = preload("res://objects/Score.tscn")
const obj_thrownscientist = preload("res://objects/ThrownScientist.tscn")

const RUNNING_SPEED = 32.0
const JUMP_SPEED = 66.0
const CLIMB_SPEED = 32.0
const FALLING_ACCEL = 128.0
const FALLING_MAX = 64.0
const ANIM_SPEED = 5.0

enum State {STANDING, JUMPING, CLIMBING, HURT}

onready var raycasts_floor = [$RayCast_Floor1, $RayCast_Floor2]
onready var raycasts_ceiling = [$RayCast_Ceiling1, $RayCast_Ceiling2]
onready var sprite = $Sprite
onready var sprite_scientist = $Sprite_Scientist
onready var collision_standing = $CollisionShape2D_Standing
onready var collision_climbing = $CollisionShape2D_Climbing
onready var audio_step = $Audio_Step
onready var audio_jump = $Audio_Jump
onready var audio_climb = $Audio_Climb
onready var audio_shock = $Audio_Shock
onready var audio_die = $Audio_Die
onready var audio_pickup = $Audio_Pickup
onready var audio_throw = $Audio_Throw

var velocity = Vector2(0.0, 0.0)
var on_ladder
var jump_high_point

onready var anim_frame = 0.0
onready var current_state = State.STANDING
onready var carrying_scientist = null

signal scientist_saved
signal player_hurt

func check_on_floor() -> bool:
	for current_raycast in raycasts_floor:
		if current_raycast.is_colliding():
			return true
	return false

func check_on_ceiling() -> bool:
	for current_raycast in raycasts_ceiling:
		if current_raycast.is_colliding():
			return true
	return false

func get_floor_colliders() -> Array:
	var results = []
	for current_raycast in raycasts_floor:
		if current_raycast.is_colliding():
			if not results.has(current_raycast.get_collider()):
				results.append(current_raycast.get_collider())
	return results

func check_for_ladders():
	for current_ladder in get_tree().get_nodes_in_group("ladder"):
		if current_ladder.overlaps_body(self):
			# Get on the ladder
			position.x = current_ladder.position.x
			current_state = State.CLIMBING
			on_ladder = current_ladder

func check_for_damage():
	for current_damager in get_tree().get_nodes_in_group("deadly"):
		if current_damager.overlaps_body(self):
			get_hurt()

func check_for_scientist():
	for current_scientist in get_tree().get_nodes_in_group("scientist"):
		if current_scientist.overlaps_body(self) and current_scientist.picked_up == false:
			current_scientist.pickup()
			sprite_scientist.show()
			carrying_scientist = current_scientist.slug
			audio_pickup.play()

func check_for_spaceship():
	for current_spaceship in get_tree().get_nodes_in_group("spaceship"):
		if current_spaceship.overlaps_body(self):
			throw_scientist()

func get_hurt():
	emit_signal("player_hurt")
	audio_shock.play()
	velocity.x = 0
	current_state = State.HURT
	get_tree().paused = true
	yield(get_tree().create_timer(0.5), "timeout")
	get_tree().paused = false
	audio_die.play()

func throw_scientist():
	var score = obj_score.instance()
	score.global_position = position + Vector2(0, -8)
	score.set_point_value(1000)
	get_tree().call_group("game", "add_score", 1000)
	get_parent().add_child(score)
	var thrown = obj_thrownscientist.instance()
	thrown.position = position + Vector2(0, -8)
	get_parent().add_child(thrown)
	sprite_scientist.hide()
	emit_signal("scientist_saved", carrying_scientist)
	carrying_scientist = null
	audio_throw.play()

func standing(delta):
	sprite.frame = int(anim_frame) % 2
	if carrying_scientist != null: sprite.frame += 5
	collision_standing.disabled = false
	if check_on_floor():
		velocity = Vector2(0.0, 0.0)
		# Check for moving platforms
		var colliders = get_floor_colliders()
		for current_collider in colliders:
			if current_collider is MovingPlatform:
				move_and_collide(current_collider.velocity * delta)
		# Running about
		if Input.is_action_pressed("run_left"):
			velocity.x = -RUNNING_SPEED
			sprite.flip_h = true
			sprite_scientist.flip_v = false
			anim_frame += delta * ANIM_SPEED
			if not audio_step.playing:
				audio_step.play()
		if Input.is_action_pressed("run_right"):
			velocity.x = RUNNING_SPEED
			sprite.flip_h = false
			sprite_scientist.flip_v = true
			anim_frame += delta * ANIM_SPEED
			if not audio_step.playing:
				audio_step.play()
		# Jumping
		if carrying_scientist == null:
			if Input.is_action_just_pressed("jump"):
				velocity.y = -JUMP_SPEED
				jump_high_point = position.y
				current_state = State.JUMPING
				audio_jump.play()
			elif Input.is_action_just_pressed("climb_up") or Input.is_action_just_pressed("climb_down"):
				check_for_ladders()
		
		var collision = move_and_collide(velocity * delta)
	else:
		jump_high_point = position.y
		current_state = State.JUMPING
	check_for_damage()
	check_for_scientist()
	if carrying_scientist != null:
		check_for_spaceship()

func jumping(delta):
	if carrying_scientist == null: sprite.frame = 2
	if position.y < jump_high_point:
		jump_high_point = position.y
	velocity.y += FALLING_ACCEL * delta
	var collision = move_and_collide(velocity * delta)
	if collision:
		if collision.normal == Vector2(0, -1):
			var fall_height = position.y - jump_high_point
			if fall_height > 80:
				velocity.y = 0.0
				get_hurt()
			else:
				current_state = State.STANDING
		if collision.normal == Vector2(0, 1):
			velocity.y = 0.0
		if collision.normal.y == 0:
			velocity.x = 0.0
	check_for_damage()

func climbing(delta):
	collision_standing.disabled = true
	sprite.frame = 3
	sprite.flip_h = (int(anim_frame) % 2 == 0)
	velocity = Vector2(0, 0)
	position.x = on_ladder.position.x
	if Input.is_action_pressed("climb_up"):
		velocity.y = -CLIMB_SPEED
		anim_frame += delta * ANIM_SPEED
		if not audio_climb.playing:
			audio_climb.play()
	if Input.is_action_pressed("climb_down"):
		velocity.y = CLIMB_SPEED
		anim_frame += delta * ANIM_SPEED
		if not audio_climb.playing:
			audio_climb.play()
	var collision = move_and_collide(velocity * delta)
	# Are we jumping off?
	if Input.is_action_just_pressed("run_left"):
		velocity.x = -RUNNING_SPEED
		velocity.y = 0
		jump_high_point = position.y
		current_state = State.JUMPING
	if Input.is_action_just_pressed("run_right"):
		velocity.x = RUNNING_SPEED
		velocity.y = 0
		jump_high_point = position.y
		current_state = State.JUMPING
	# Make sure we're still on a ladder
	if position.y <= on_ladder.position.y - 16:
		velocity.y = 0
		position.y = on_ladder.position.y - 16
		current_state = State.STANDING
	check_for_damage()

func dying(delta):
	collision_standing.disabled = true
	collision_climbing.disabled = true
	sprite.frame = 4
	velocity.y += FALLING_ACCEL * delta
	velocity.y = clamp(velocity.y, -FALLING_MAX, FALLING_MAX * 2.0)
	position += velocity * delta
	if position.y > 320:
		get_parent().reset()

func _physics_process(delta):
	match current_state:
		State.STANDING:
			standing(delta)
		State.JUMPING:
			jumping(delta)
		State.CLIMBING:
			climbing(delta)
		State.HURT:
			dying(delta)
		
