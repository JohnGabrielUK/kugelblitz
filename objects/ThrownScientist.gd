extends Sprite

onready var velocity = Vector2(48.0, -32.0)

func _physics_process(delta):
	velocity.y += delta * 64
	position += velocity * delta

func _on_Timer_timeout():
	queue_free()
