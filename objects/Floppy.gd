extends Area2D

const obj_score = preload("res://objects/Score.tscn")

onready var audio_bonus = $Audio_Bonus

onready var picked_up = false
onready var despawn_timer = 20

func _physics_process(delta):
	if not picked_up:
		despawn_timer -= delta
		if despawn_timer < 0:
			queue_free()
		elif despawn_timer < 10:
			# Blinking
			if int(despawn_timer*5) % 2 == 0:
				hide()
			else:
				show()

func _on_Floppy_body_entered(body):
	if picked_up: return
	if body is Player:
		picked_up = true
		hide()
		audio_bonus.play()
		var score = obj_score.instance()
		score.global_position = global_position + Vector2(0, -16)
		score.set_point_value(1000)
		get_tree().call_group("game", "add_score", 1000)
		get_parent().add_child(score)
		yield(audio_bonus, "finished")
		queue_free()
