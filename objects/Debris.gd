extends Sprite

var destination : Vector2
onready var velocity = Vector2(0, 0)
var delay

func set_region(region):
	region_rect = region

func set_flipped(h_flip, v_flip):
	flip_h = h_flip
	flip_v = v_flip

func _physics_process(delta):
	if delay > 0:
		delay -= delta
		if delay < 1:
			offset.y = randf()
	else:
		if position.distance_to(destination) < 8:
			queue_free()
		else:
			var direction = position.angle_to_point(destination)
			velocity.x = cos(direction) * 128
			velocity.y = sin(direction) * 128
			# whirlwind effect
			velocity.x += sin(direction) * 256
			velocity.y -= cos(direction) * 256
			#velocity *= 1 - (delta/3)
			position -= velocity * delta