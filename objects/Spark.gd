extends Area2D

const obj_score = preload("res://objects/Score.tscn")

const ANIM_SPEED = 5.0
const PATH_SPEED = 32.0

export (float) var anim_frame = 0.0

export (NodePath) var path_follower
onready var path_follow = get_node(path_follower)

onready var sprite = $Sprite
onready var audio_bonus = $Audio_Bonus

onready var jumped = false

func _physics_process(delta):
	anim_frame += delta * ANIM_SPEED
	sprite.frame = int(anim_frame) % sprite.hframes
	path_follow.offset += PATH_SPEED * delta
	global_position = path_follow.global_position

func _on_Area2D_JumpBonus_body_entered(body):
	if body is Player and !jumped:
		audio_bonus.play()
		var score = obj_score.instance()
		score.global_position = global_position + Vector2(0, -16)
		score.set_point_value(200)
		get_tree().call_group("game", "add_score", 200)
		get_parent().add_child(score)
		jumped = true
