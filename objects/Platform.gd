class_name MovingPlatform
extends KinematicBody2D

const PATH_SPEED = 32.0

export (NodePath) var path_follower
onready var path_follow = get_node(path_follower)

var last_position = Vector2(0, 0)
var velocity

func _physics_process(delta):
	path_follow.offset += PATH_SPEED * delta
	last_position = global_position
	global_position = path_follow.global_position
	velocity = (global_position - last_position) / delta