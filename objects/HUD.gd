extends CanvasLayer

onready var label_score = $Control_ScreenSize/Label_Score
onready var label_timer = $Control_ScreenSize/Label_Timer
onready var label_hiscore = $Control_ScreenSize/Label_Hiscore
onready var texture_lives = $Control_ScreenSize/TextureRect_Lives

func set_label_score(value):
	label_score.text = "%06d" % value
	
func set_label_timer(value):
	label_timer.text = "%03d" % max(0, value)

func set_label_hiscore(value):
	label_hiscore.text = "%06d" % value

func set_label_lives(value):
	if value > 0:
		texture_lives.show()
		texture_lives.rect_size.x = value*12
	else:
		texture_lives.hide()
