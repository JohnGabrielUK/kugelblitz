extends Area2D
class_name Chaser

const FALLING_ACCEL = 128.0
const FALLING_MAX = 64.0

const STATE_ON_FLOOR_IDLING = 0
const STATE_ON_FLOOR_CHASING = 1
const STATE_GOTO_WALL = 2
const STATE_CLIMB_WALL = 3
const STATE_LUNGE = 4

onready var sprite = $Sprite
onready var timer_changemind = $Timer_ChangeMind
onready var audio_bark = $Audio_Bark

export (NodePath) var path_player
onready var player = get_node(path_player)

onready var velocity = Vector2(0, 0)

onready var state := STATE_ON_FLOOR_IDLING
onready var which_wall = 0 # -1 for left, 1 for right
onready var wait_index = 0

var speed_multiplier = 1

func change_mind() -> void:
	match state:
		STATE_ON_FLOOR_IDLING:
			# Is the player on the same floor as us?
			if player.position.y > 224:
				state = STATE_ON_FLOOR_CHASING
				audio_bark.play()
				# Are we chasing left or right?
				if player.position.x > position.x:
					which_wall = 1
				else:
					which_wall = -1
			# Is the player on the side platforms?
			elif player.position.x < 40:
				which_wall = -1
				state = STATE_GOTO_WALL
			elif player.position.x > 184:
				which_wall = 1
				state = STATE_GOTO_WALL
		STATE_ON_FLOOR_CHASING:
			# Is the player no longer on the same floor?
			if player.position.y < 224:
				state = STATE_ON_FLOOR_IDLING
			elif position.x < 24:
				which_wall = 1
			elif position.x > 200:
				which_wall = -1
		STATE_GOTO_WALL:
			# Is the player on the same floor as us?
			if player.position.y > 224:
				state = STATE_ON_FLOOR_CHASING
				audio_bark.play()
				# Are we chasing left or right?
				if player.position.x > position.x:
					which_wall = 1
				else:
					which_wall = -1
			# Have we reached the wall?
			if position.x <= 15 && which_wall == -1:
				state = STATE_CLIMB_WALL
			elif position.x >= 209 && which_wall == 1:
				state = STATE_CLIMB_WALL
		STATE_CLIMB_WALL:
			var want_to_lunge = false
			if player.position.y - 16 > position.y and ((player.position.x < 64 and which_wall == -1) or (player.position.x > 176 and which_wall == 1)):
				want_to_lunge = true
			if player.position.y > 224:
				want_to_lunge = true
			if want_to_lunge:
				state = STATE_LUNGE
				sprite.frame = 6
				audio_bark.play()
				if which_wall == -1:
					velocity = Vector2(32, 0)
					sprite.flip_h = false
				else:
					velocity = Vector2(-32, 0)
					sprite.flip_h = true

func on_floor_idling(delta : float) -> void:
	pass

func on_floor_chasing(delta : float) -> void:
	if which_wall == -1:
		sprite.flip_h = true
		if position.x > 24:
			position.x -= delta * 24 * speed_multiplier
			sprite.frame = int(wait_index*5) % 2
	elif which_wall == 1:
		sprite.flip_h = false
		if position.x < 200:
			position.x += delta * 24 * speed_multiplier
			sprite.frame = int(wait_index*5) % 2

func going_to_wall(delta : float) -> void:
	if which_wall == -1:
		sprite.flip_h = true
		if position.x > 14:
			position.x -= delta * 24 * speed_multiplier
			sprite.frame = int(wait_index*5) % 2
		else:
			sprite.frame = 2
	elif which_wall == 1:
		sprite.flip_h = false
		if position.x < 210:
			position.x += delta * 24 * speed_multiplier
			sprite.frame = int(wait_index*5) % 2
		else:
			sprite.frame = 2

func climbing_wall(delta : float) -> void:
	if position.y > player.position.y - 16:
		position.y -= delta * 8 * speed_multiplier
		sprite.frame = 3 + (int(wait_index*5) % 2)
	else:
		sprite.frame = 5

func lunging(delta : float) -> void:
	velocity.y += FALLING_ACCEL * delta
	position += velocity * delta
	if position.y > 262:
		position.y = 262
		state = STATE_ON_FLOOR_IDLING
		sprite.frame = 0

func _physics_process(delta : float) -> void:
	wait_index += delta
	match state:
		STATE_ON_FLOOR_IDLING: on_floor_idling(delta)
		STATE_ON_FLOOR_CHASING: on_floor_chasing(delta)
		STATE_GOTO_WALL: going_to_wall(delta)
		STATE_CLIMB_WALL: climbing_wall(delta)
		STATE_LUNGE: lunging(delta)

func _on_Timer_ChangeMind_timeout():
	change_mind()

func _ready():
	speed_multiplier = min(1 + (Overall.level/2), 3)
	timer_changemind.wait_time = max(3 - (Overall.level * 0.5), 0.5)
