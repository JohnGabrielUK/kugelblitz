extends Sprite

var frames = {
	100: 0,
	200: 1,
	500: 2,
	1000: 3,
	2000: 4,
	5000: 5
}

func set_point_value(points):
	frame = frames[points]

func _physics_process(delta):
	position.y -= delta * 16

func _on_Timer_timeout():
	queue_free()
