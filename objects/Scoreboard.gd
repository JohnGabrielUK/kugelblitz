extends Control

onready var label_scoreboard = $Label_Scoreboard

func display() -> void:
	make_rankings_list()
	label_scoreboard.visible_characters = 0

func make_rankings_list() -> void:
	var text = "RANK  SCORE  NAME"
	for i in range(0, 9):
		var ranking = Rankings.rankings[i]
		var pos
		if i > 2:
			pos = str(i+1) + "TH"
		else:
			match i:
				0: pos = "1ST"
				1: pos = "2ND"
				2: pos = "3RD"
		var name = ranking.name
		var score = ranking.score
		var line = "\n%s  %06d  %s" % [pos, score, name]
		text = text + line
	label_scoreboard.text = text

func _on_Timer_Typewriter_timeout() -> void:
	label_scoreboard.visible_characters += 1
