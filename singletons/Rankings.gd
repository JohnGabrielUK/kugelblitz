extends Node

class Ranking:
	var name : String
	var score : int
	
	func _init(name, score) -> void:
		self.name = name
		self.score = score
	
	static func sort(a : Ranking, b : Ranking) -> bool:
		if a.score > b.score:
			return true
		if a.score == b.score:
			if a.name.nocasecmp_to(b.name) == -1:
				return true
		return false

var rankings : Array

func get_highest_ranking_score() -> int:
	var top_ranking = rankings.front()
	return top_ranking.score

func get_lowest_ranking_score() -> int:
	var last_ranking = rankings.back()
	return last_ranking.score

func score_makes_ranking(score : int) -> bool:
	var score_to_beat = get_lowest_ranking_score()
	if score > get_lowest_ranking_score():
		return true
	return false

func sort_rankings() -> void:
	rankings.sort_custom(Ranking, "sort")
	rankings.resize(9)

func add_ranking(ranking: Ranking) -> void:
	rankings.append(ranking)
	sort_rankings()

func _enter_tree() -> void:
	rankings.append(Ranking.new("JGB", 10000))
	rankings.append(Ranking.new("WNG", 10000))
	rankings.append(Ranking.new("DLT", 10000))
	rankings.append(Ranking.new("GLD", 10000))
	rankings.append(Ranking.new("REG", 10000))
	rankings.append(Ranking.new("AEM", 10000))
	rankings.append(Ranking.new("PHL", 10000))
	rankings.append(Ranking.new("KRY", 10000))
	rankings.append(Ranking.new("CRL", 20000))
	sort_rankings()
