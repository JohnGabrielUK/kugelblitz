extends Node

class Level:
	var scientists_to_save : Array
	
	func _init(scientists_to_save : Array):
		self.scientists_to_save = scientists_to_save

var levels = {
	0: Level.new(["1", "2"])
}

var level : int
var lives : int
var scientists_saved
var timer

var next_bonus # At what time will the next bonus floppy disk spawn?
var next_extra_life # At what score will we award the player a bonus life?

var score : int setget score_updated
var hiscore : int # Bit of a hack for the sake of the in-game display
var rankings : Array
onready var credits := 0

var audio_coinin

signal credit_added

func _unhandled_input(event) -> void:
	if event.is_action_pressed("insert_coin"):
		get_tree().set_input_as_handled()
		audio_coinin.play()
		credits += 1
		emit_signal("credit_added")

func score_updated(value : int) -> void:
	score = value
	if score > next_extra_life:
		next_extra_life *= 2
		audio_coinin.play()
		lives += 1
	if score > Rankings.get_highest_ranking_score():
		hiscore = score

func start_level():
	var level_to_start = levels[level % levels.size()]
	# Set up scientists
	scientists_saved = {}
	for current_scientist in level_to_start.scientists_to_save:
		scientists_saved[current_scientist] = false
	# Set up timer
	timer = max(250, 500-(level*50))
	next_bonus = timer - 100
	get_tree().change_scene("res://scenes/Game.tscn")

func next_level() -> void:
	level += 1
	start_level()

func new_game() -> void:
	level = 0
	lives = 2
	score = 0
	next_extra_life = 10000
	hiscore = Rankings.get_highest_ranking_score()
	start_level()

func _ready() -> void:
	audio_coinin = AudioStreamPlayer.new()
	audio_coinin.stream = load("res://sounds/coin_entered.wav")
	add_child(audio_coinin)
	hiscore = Rankings.get_highest_ranking_score()
